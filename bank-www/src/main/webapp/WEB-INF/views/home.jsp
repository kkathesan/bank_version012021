<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>Bienvenue</title>
	<link
		href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
		rel="stylesheet"
		integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
		crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<!-- <h1>Bienvenue sur la page web </h1> -->
			<!-- <h1>Assurance Bank System</h1> -->
			<div class="col-sm-12"><h1 id="name">Assurance Bank System </h1></div>
				
				<nav class="navbar navbar-dark bg-primary">
					<div class="topnav">
						<div class="row">
							<div class="col-sm-2"><a class="active" href="/java/home/">Accueil</a> </div>
							<div class="col-sm-2"><div class="row"><a href="#">Comptes & Cartes</a> </div></div>
							<div class="col-sm-2"><div class="row"><a href="#">Épargner</a></div></div>
							<div class="col-sm-2"><div class="row"><a href="#">Emprunter</a> </div></div>
							<div class="col-sm-2"><div class="row"><a href="#">Nos conseils</a></div></div>
							<!-- <a href="/java/client/doTransfer">Effectuer un virement</a>  -->
							<div class="col-sm-2"><div class="row"><a href="#">Nous contacter</a></div></div>
						</div>
					</div>
				</nav>
				
				<P>  The time on the server is ${serverTime}. </P>
				<!-- <P><a href="/java/admin"> Connexion </a></P> -->
		
				<center><div class="row bouton"><span class="encadrer-un-contenu"><a href="/java/admin" id="connexion"> Connexion </a></span></div></center>
				
				<div class="">
					<a href="#">
						<center><img src="<c:url value="/resources/banque.jpg" />" alt="image d'accueil"/class="img-responsive center"></center>
					</a>
				</div>
				
				<P>
					Notre banque est un établissement de crédit qui a reçu un agrément de la part de l'Autorité de contrôle prudentiel et de résolution.</br>

					Le rôle principal de Assurance Bank System consiste à assurer la gestion des moyens de paiement. </br>
		
					La définition traditionnelle des banques est concurrencée par celle de « néo-banque » (ou banque digitale) dont les produits et les offres ne sont accessibles qu’à travers un contact numérique. </br>
				
					Nous pouvons aussi réaliser des opérations sur métaux précieux, délivrer un conseil en matière de gestion de patrimoine et assurer le placement, la souscription, l'achat, la gestion, la garde et la vente de valeurs mobilières et de produits financiers ou immobiliers. </br> 
					
					Enfin, nous sommes aussi habilités à émettre et gérer des monnaies électroniques et à proposer tous les services destinés à faciliter la création et le développement des entreprises. </br>
				</P>
				
				<hr class="trait">

<footer>
	<section class="section footer-section footer-section-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div id="c69" class="frame frame-default frame-type-html frame-layout-0 frame-space-before-none frame-space-after-none">
						<div class="limayrac">
							<h4 class="text-center">Institut LIMAYRAC</h4>
							<p>50 Rue de Limayrac
							<br>
							31000 Toulouse</p>
							<p>05 63 00 00 00
							<br>
  							david.creton@limayrac.fr</p>
  							<center>
 								<a href="#">
  									<img src="" class="img-responsive logobas 1">
  								</a>
  							</center>
  						</div>
  					</div>
 				</div>
  				<div class="col-sm-4 horaires">
  					<div id="c73" class="frame frame-default frame-type-html frame-layout-0 frame-space-before-none frame-space-after-none">
  						<div class="limayrac">
  							<!-- <hr class="traitaccueil"> -->
  								<div class="barres">
  									<h4 class="text-center">HORAIRES</h4>
  									<p>Du lundi au vendredi : 
	  									<br>
	  									8h00 - 12h00 
	 									<br>
	 									13h00 - 18h00
 									</p>
 									<br>
 								</div>
 								<center>
 									<img src="" class="img-responsive logobas 2">
 								</center>
 								<!--<hr class="traitaccueil"> -->
 						</div>
 					</div>
 				</div>
 				<div class="col-sm-4">
 					<div id="c74" class="frame frame-default frame-type-html frame-layout-0 frame-space-before-none frame-space-after-none">
 						<div class="services">
 							<h4 class="text-center">SERVICES TECHNIQUES</h4>
 							<p>50 Rue de Limayrac 
 							<br> 
 							David CRETON
  							<br>
  							31000 Toulouse
  							</p>
  							<p>05 63 00 00 00</p>
  							<center>
  								<a href="#" title="Page banque" target="_blank">
  									<img src="" class="img-responsive logobas 3">
  								</a>
  							</center>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</section>
  	<section class="section section-small footer-section footer-section-meta">
  		<div class="container">
	  		<div class="frame frame-small default meta">
	  		</div>
	  		<div class="frame frame-small default copyright">
		  		<p id="footer">© Élève de master 2 ESI 
		  			<span class="trait"> | </span>
		  			<a href="https://choose-your-stream.fsil.fr/" title="Plan du site" class="plan"> Plan du site </a> | <a href="https://choose-your-stream.fsil.fr/" title="Mentions légales" class="mentions"> Mentions légales </a> | Conception <a href="https://choose-your-stream.fsil.fr/" title="Site chooseyourstream" target="_blank">Kathesan</a>
		  		</p>
	  		</div>
  		</div>
  	</section>
</footer>
		
				<!-- <P><a href="/java/admin"> Connexion admin</a></P> -->
				<!-- <P><a href="/java/user"> Connexion user</a></P> -->
		</div>
	</div>
<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"
		integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"
		integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj"
		crossorigin="anonymous"></script>

</body>

<style>
h1{
	font-family: arial;
	/*background-color:#dddddd;
	text-align:center;*/
}
a{
	text-align:center;
	/*margin-left: 2em;*/
	color: black;
}
p{
	text-align: center;
}
/*.encadrer-un-contenu{ border: 1px solid black; padding: 5px; text-align: center;}*/
.row.bouton {
    text-align: center;
    border: 1px solid black;
    padding: 5px;
    width: 20%;
    margin-bottom: 30px;
}
h1#name {
    /*background-color: cornflowerblue;*/
}
hr.trait {
    /*width: 50%;*/
    border: 1px solid;
    margin-top: 50px;
    margin-bottom: 50px;
}
p#footer {
    margin-top: 10px;
}
img.img-responsive.center {
    margin-bottom: 30px;
}
</style>

</html>
