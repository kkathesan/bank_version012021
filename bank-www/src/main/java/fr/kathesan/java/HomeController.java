package fr.kathesan.java;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "/home/", method = RequestMethod.GET)
	public String homePage(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "/admin/**", method = RequestMethod.GET) //@RequestMapping(value = "/admin/**", method = RequestMethod.GET)
	public String adminPage(Model model) {
		
		model.addAttribute("message", "Page d'administration" );
		
		return "accueil";	//return "admin";
	}
	
	@RequestMapping(value = "/user/**", method = RequestMethod.GET) 
	public String userPage(Model model) {
		
		model.addAttribute("message", "Page user" );
		
		return "accueilUser";	//return "user";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutProcess(HttpSession session) {
        session.invalidate();
        System.out.println("REDIRECT: from {[GET] '/logout'} to {[GET] '/'}");
        return "redirect:/home/";
    }
	
}
