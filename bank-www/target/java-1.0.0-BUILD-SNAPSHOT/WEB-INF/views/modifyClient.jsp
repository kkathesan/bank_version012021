<html>
<style>
form {
	font-family: arial;
	background-color: #dddddd;
	margin-bottom: 2em;
	margin-top: 2em;
	/*text-align:center;*/
	padding-top: 2em;
	padding-bottom: 1em;
}

.topnav {
	text-align: center;
}

a {
	/*font-family: arial;*/
	/*background-color:#dddddd;*/
	/*margin-bottom:2em;*/
	/*margin-top:2em;*/
	/*text-align:center;*/
	/*padding-left:2em;*/
	margin-left: 2em;
	/*background-color: lightgrey;*/
	color: black;
}

h1 {
	font-family: arial;
	background-color: #dddddd;
	text-align: center;
}

/*div#nom {
    text-align: center;
}
div#prenom {
    margin-left: 485;
}
div#adresse {
    margin-left: 482;
}
div#code_postal {
    margin-left: 455;
}
div#ville {
    margin-left: 512;
}
div#mot_de_passe {
    margin-left: 447;
}
div#valider {
    margin-top: 50;
    margin-left: 640;
}*/
div#valider {
	margin-left: 2em;
}

p {
	text-align: center;
}
a.active {
    background-color: grey;
}
</style>
<head>
	<title>Modifier client</title>
	<link
		href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
		rel="stylesheet"
		integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
		crossorigin="anonymous">
	</head>
<body>
	<div class="container">
		<nav class="navbar navbar-dark bg-primary">
			<div class="topnav">
				<div class="row">
					<div class="col-sm-2"><a href="/java/admin/accueil">Accueil</a></div> 
					<div class="col-sm-2"><div class="row"><a class="active" href="/java/client/add">Ajouter un client</a></div></div> 
					<div class="col-sm-2"><div class="row"><a href="/java/admin/client/listClients">Liste des clients/comptes</a></div></div> 
					<div class="col-sm-2"><div class="row"><a href="/java/admin/client/listClientSolo">Liste des clients</a></div></div> 
					<div class="col-sm-2"><div class="row"><a href="/java/admin/client/listAccountSolo">Liste des comptes</a></div></div>
					<!-- <a href="/java/client/doTransfer">Effectuer un virement</a> -->
					<div class="col-sm-2"><a href="<c:url value="/logout" />">D�connexion</a></div>
				</div>
			</div>
		</nav>
	
		<h1>Modifier un client</h1>
	
		<P>The time on the server is ${serverTime}.</P>
	
		<form method="POST">
			<div id=nom>
				<a>Nom : </a><input type="text" name="nom" value="${client_form.surname}" />
				<p>
			</div>
			<div id=prenom>
				<a>Prenom : </a><input type="text" name="prenom" value="${client_form.forename}" />
				<p>
			</div>
			<div id=adresse>
				<a>Adresse : </a><input type="text" name="adresse" value="${client_form.address}" />
				<p>
			</div>
			<div id=code_postal>
				<a>Code postal : </a><input type="text" name="codepostal" value="${client_form.zip}" />
				<p>
			</div>
			<div id=ville>
				<a>Ville : </a><input type="text" name="ville" value="${client_form.city }" />
				<p>
			</div>
			<div id=mot_de_passe>
				<a>Mot de passe : </a><input type="text" name="mot de passe" value="${client_form.password }" />
				<p>
			</div>
			<div id=valider>
				<input type="submit" name="ajouter" />
				<p>
			</div>
		</form>
		
		<script
			src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
			integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
			crossorigin="anonymous"></script>
		<script
			src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"
			integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU"
			crossorigin="anonymous"></script>
		<script
			src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"
			integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj"
			crossorigin="anonymous"></script>
			
			<hr class="trait">

<footer>
	<section class="section footer-section footer-section-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div id="c69" class="frame frame-default frame-type-html frame-layout-0 frame-space-before-none frame-space-after-none">
						<div class="limayrac">
							<h4 class="text-center">Institut LIMAYRAC</h4>
							<p>50 Rue de Limayrac
							<br>
							31000 Toulouse</p>
							<p>05 63 00 00 00
							<br>
  							david.creton@limayrac.fr</p>
  							<center>
 								<a href="#">
  									<img src="" class="img-responsive logobas 1">
  								</a>
  							</center>
  						</div>
  					</div>
 				</div>
  				<div class="col-sm-4 horaires">
  					<div id="c73" class="frame frame-default frame-type-html frame-layout-0 frame-space-before-none frame-space-after-none">
  						<div class="limayrac">
  							<!-- <hr class="traitaccueil"> -->
  								<div class="barres">
  									<h4 class="text-center">HORAIRES</h4>
  									<p>Du lundi au vendredi : 
	  									<br>
	  									8h00 - 12h00 
	 									<br>
	 									13h00 - 18h00
 									</p>
 									<br>
 								</div>
 								<center>
 									<img src="" class="img-responsive logobas 2">
 								</center>
 								<!--<hr class="traitaccueil"> -->
 						</div>
 					</div>
 				</div>
 				<div class="col-sm-4">
 					<div id="c74" class="frame frame-default frame-type-html frame-layout-0 frame-space-before-none frame-space-after-none">
 						<div class="services">
 							<h4 class="text-center">SERVICES TECHNIQUES</h4>
 							<p>50 Rue de Limayrac 
 							<br> 
 							David CRETON
  							<br>
  							31000 Toulouse
  							</p>
  							<p>05 63 00 00 00</p>
  							<center>
  								<a href="#" title="Page banque" target="_blank">
  									<img src="" class="img-responsive logobas 3">
  								</a>
  							</center>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</section>
  	<section class="section section-small footer-section footer-section-meta">
  		<div class="container">
	  		<div class="frame frame-small default meta">
	  		</div>
	  		<div class="frame frame-small default copyright">
		  		<p id="footer">� �l�ve de master 2 ESI 
		  			<span class="trait"> | </span>
		  			<a href="https://choose-your-stream.fsil.fr/" title="Plan du site" class="plan"> Plan du site </a> | <a href="https://choose-your-stream.fsil.fr/" title="Mentions l�gales" class="mentions"> Mentions l�gales </a> | Conception <a href="https://choose-your-stream.fsil.fr/" title="Site chooseyourstream" target="_blank">Kathesan</a>
		  		</p>
	  		</div>
  		</div>
  	</section>
</footer>
	</div>
</body>
</html>