package fr.jdmassard.java.bankmanagerapp.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.dao.ClientDAO;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Client;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

@TestMethodOrder(OrderAnnotation.class)
class TestHibernateClientDAO {

	// Data mocks
	private Client clientA = null;
	private Client clientB = null;

	private ClientDAO clientDAO = null;
	private ApplicationContext context = null;

	@BeforeEach
	void setUp() throws Exception {

		if (context == null)
			context = new ClassPathXmlApplicationContext("bank-database-test-context.xml");

		if (clientDAO == null)
			clientDAO = (ClientDAO) context.getBean("clientDAO");

		// Setup clientA mock
		if (clientA == null) {
			clientA = new Client();
			clientA.setAddress("clientA_address");
			clientA.setCity("clientA_city");
			clientA.setForename("clientA_forename");
			clientA.setSurname("clientA_surname");
			clientA.setZip("clientA_zip");
		}

		// Setup clientB mock
		if (clientB == null) {
			clientB = new Client();
			clientB.setAddress("clientB_address");
			clientB.setCity("clientB_city");
			clientB.setForename("clientB_forename");
			clientB.setSurname("clientB_surname");
			clientB.setZip("clientB_zip");
		}
	}

	@Test
	@Order(1)
	void testCreateClient() {
		try {
			// Prepare some variables for future tests
			long initialClientAIdentifier = clientA.getId();
			long initialClientBIdentifier = clientB.getId();

			// Add clients to virtual database
			clientDAO.createClient(clientA);
			clientDAO.createClient(clientB);
			System.out.println(clientDAO.fetchClients());

			// Run tests on clientA
			assertNotEquals(initialClientAIdentifier, clientA.getId());
			assertEquals(1, clientA.getId());

			// Run tests on clientB
			assertNotEquals(initialClientBIdentifier, clientB.getId());
			assertEquals(2, clientB.getId());
		} catch (BankException e) {
			e.printStackTrace();
			fail("An error occured while adding a client to the database.");
		}
	}

	@Test
	@Order(2)
	void testSearchClientByIdentifier() {
		try {
			System.out.println(clientDAO.fetchClients());
			// Fetch some clients from the database
			clientA = clientDAO.searchClientByIdentifier((long) 1);
			clientB = clientDAO.searchClientByIdentifier((long) 2);
			Client clientC = clientDAO.searchClientByIdentifier((long) 3);

			// Run tests on clientA
			assertNotNull(clientA);
			assertEquals("clientA_address", clientA.getAddress());
			assertEquals("clientA_city", clientA.getCity());
			assertEquals("clientA_forename", clientA.getForename());
			assertEquals("clientA_surname", clientA.getSurname());
			assertEquals("clientA_zip", clientA.getZip());

			// Run tests on clientB
			assertNotNull(clientB);
			assertEquals("clientB_address", clientB.getAddress());
			assertEquals("clientB_city", clientB.getCity());
			assertEquals("clientB_forename", clientB.getForename());
			assertEquals("clientB_surname", clientB.getSurname());
			assertEquals("clientB_zip", clientB.getZip());

			// Run tests on non-existent clientC
			assertNull(clientC);
		} catch (BankException e) {
			e.printStackTrace();
			fail("An error occured while fetching a client by identifier.");
		}
	}

	@Test
	@Order(3)
	void testFetchClients() {
		try {
			// Fetch all clients form the database
			List<Client> clients = clientDAO.fetchClients();

			// Run tests on all clients
			assertNotNull(clients);
			assertEquals(2, clients.size());

			// Fetch each client from clients
			clientA = clients.get(0);
			clientB = clients.get(1);

			// Run tests on clientA
			assertNotNull(clientA);
			assertEquals(1, clientA.getId());
			assertEquals("clientA_address", clientA.getAddress());
			assertEquals("clientA_city", clientA.getCity());
			assertEquals("clientA_forename", clientA.getForename());
			assertEquals("clientA_surname", clientA.getSurname());
			assertEquals("clientA_zip", clientA.getZip());

			// Run tests on clientB
			assertNotNull(clientB);
			assertEquals(2, clientB.getId());
			assertEquals("clientB_address", clientB.getAddress());
			assertEquals("clientB_city", clientB.getCity());
			assertEquals("clientB_forename", clientB.getForename());
			assertEquals("clientB_surname", clientB.getSurname());
			assertEquals("clientB_zip", clientB.getZip());
		} catch (BankException e) {
			e.printStackTrace();
			fail("An error occured while fetching all clients.");
		}
	}

	@Test
	@Order(4)
	void testUpdateClient() {
		try {
			// Fetch initial clientA
			clientA = clientDAO.searchClientByIdentifier((long) 1);

			// Test initial clientA
			assertNotNull(clientA);

			// Update clientA
			clientDAO.updateClient(clientA);

			// Re-fetch accountA1
			clientA = clientDAO.searchClientByIdentifier((long) 1);

			// Re-test accountA1
			assertNotNull(clientA);

			// Reset to classic value
			clientDAO.updateClient(clientA);
		} catch (BankException e) {
			e.printStackTrace();
			fail("An error occured while updating an account.");
		}
	}

	@Test
	@Order(5)
	void testDeleteClient() {
		fail("Not yet implemented");
	}

}
