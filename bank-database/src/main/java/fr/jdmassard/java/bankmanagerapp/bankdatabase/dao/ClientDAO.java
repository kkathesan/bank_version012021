package fr.jdmassard.java.bankmanagerapp.bankdatabase.dao;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Client;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

/**
 * Basic interface for client data access object.
 * @author Jean-Daniel MASSARD
 */
public interface ClientDAO {

	/**
	 * Creates a new client in the database.
	 * @param client The client to create itself.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
	public abstract void createClient(Client client)
		throws BankException;
	
	/**
	 * Updates an existing client in the database.
	 * @param client The client to update itself.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
	public abstract void updateClient(Client client)
		throws BankException;
	
	/**
	 * Deletes an existing client from the database.
	 * @param account The client to delete itself.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
	public abstract void deleteClient(Client client)
		throws BankException;
	
	/**
	 * Fetches a specific client by identifier.
	 * @param id The client identifier to fetch.
	 * @return Returns the specific client.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract Client searchClientByIdentifier(Long id)
		throws BankException;
	
	/**
	 * Fetches all clients from the database.
	 * @return Returns a list of clients.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract List<Client> fetchClients()
		throws BankException;
	
}
