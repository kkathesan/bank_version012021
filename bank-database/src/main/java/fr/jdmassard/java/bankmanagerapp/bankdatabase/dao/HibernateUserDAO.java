package fr.jdmassard.java.bankmanagerapp.bankdatabase.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.User;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

public class HibernateUserDAO implements UserDAO {

	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

  @Override
  public void createUser(User user) throws BankException {
    try {
      sessionFactory.getCurrentSession().persist(user);
    } catch (HibernateException exception) {
      throw new BankException("Cannot create the new user in database.");
    }
  }

  @Override
  public void updateUser(User user) throws BankException {
    try {
      sessionFactory.getCurrentSession().merge(user);
    } catch (HibernateException exception) {
      throw new BankException("Cannot update user with username" + user.getUsername() + ".");
    }
  }

  @Override
  public void deleteUser(User user) throws BankException {
    try {
      sessionFactory.getCurrentSession().delete(user);
    } catch (HibernateException exception) {
      throw new BankException("Cannot delete the user from the database.");
    }
  }

  @Override
  public User fetchUserByUsername(String username) throws BankException {
    try {
      return (User) sessionFactory.getCurrentSession()
          .createQuery("from User as u where u.username=:username", User.class).setParameter("username", username)
          .list().get(0);
    } catch (HibernateException exception) {
      throw new BankException("Cannot load user with username: " + username + " from database.");
    }
  }

  @Override
  public List<User> fetchUsers() throws BankException {
    try {
      return sessionFactory.getCurrentSession().createQuery("from User as u").list();
    } catch (HibernateException exception) {
      throw new BankException("An error occured while fetching all users from database.");
    }
  }

}
