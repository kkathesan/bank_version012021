package fr.jdmassard.java.bankmanagerapp.bankdatabase.dao;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Authority;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

public interface AuthorityDAO {

  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
  public abstract void createAuthority(Authority authority) throws BankException;

  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
  public abstract void updateAuthority(Authority authority) throws BankException;

  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
  public abstract void deleteAuthority(Authority authority) throws BankException;

  @Transactional(propagation = Propagation.REQUIRED)
  public abstract List<Authority> fetchAuthoritiesByUsername(String username) throws BankException;

}
