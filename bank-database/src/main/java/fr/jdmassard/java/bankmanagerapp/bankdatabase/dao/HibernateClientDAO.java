package fr.jdmassard.java.bankmanagerapp.bankdatabase.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Client;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

/**
 * Implementation of client data access object.
 * @author Jean-Daniel MASSARD
 */
public class HibernateClientDAO implements ClientDAO {
	
	/**
	 * Reference to the current session factory.
	 */
	private SessionFactory sessionFactory;

	/**
	 * Automatically sets the session factory from the bean configuration.
	 * @param sessionFactory Reference to the session factory itself.
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * Creates a new client in the database.
	 * @param client The client to create itself.
	 * @throws BankException
	 */
	public void createClient(Client client) throws BankException {
		try {
			sessionFactory.getCurrentSession().persist(client);
		} catch (HibernateException exception) {
			throw new BankException("Cannot persist client into the database.");
		}
	}
	
	/**
	 * Deletes an existing client from the database.
	 * @param account The client to delete itself.
	 * @throws BankException
	 */
	public void deleteClient(Client client) throws BankException {
		try {
			sessionFactory.getCurrentSession().delete(client);
		} catch (HibernateException exception) {
			throw new BankException("Cannot delete the client from the database.");
		}
	}

	/**
	 * Fetches a specific client by identifier.
	 * @param id The client identifier to fetch.
	 * @return Returns the specific client.
	 * @throws BankException
	 */
	public Client searchClientByIdentifier(Long id) throws BankException {
		try {
			return sessionFactory.getCurrentSession().get(Client.class, id);
		} catch (HibernateException exception) {
			throw new BankException("Cannot load client with id: " + id + " from database.");
		}
	}

	/**
	 * Fetches all clients from the database.
	 * @return Returns a list of clients.
	 * @throws BankException
	 */
	@SuppressWarnings("unchecked")
	public List<Client> fetchClients() throws BankException {
		try {
			String query = "from Client as c";
			return sessionFactory.getCurrentSession().createQuery(query).list();
		} catch (HibernateException exception) {
			throw new BankException("An error occured while fetching all clients from database.");
		}
	}

	/**
	 * Updates an existing client in the database.
	 * @param client The client to update itself.
	 * @throws BankException
	 */
	public void updateClient(Client client) throws BankException {
		try {
			sessionFactory.getCurrentSession().merge(client);
		} catch (HibernateException exception) {
			throw new BankException("Cannot update client n�" + client.getId() + ".");
		}
	}

}
