
package fr.jdmassard.java.bankmanagerapp.bankdatabase.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Basic class entity for client.
 * 
 * @author Jean-Daniel MASSARD
 */
@Entity
@Table(name = "client")
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "surname")
	private String surname;

	@Column(name = "forename")
	private String forename;

	@Column(name = "address")
	private String address;

	@Column(name = "zip")
	private String zip;

	@Column(name = "city")
	private String city;

	@OneToOne
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Account> accounts;

	public List<Account> getAccounts() {
		return accounts;
	}

	public long getId() {
		return id;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String toString() {
		return String.format("Client[id=%d, lastname='%s', firstname='%s', address='%s', zip='%s', city='%s']", id,
		        surname, forename, address, zip, city);
	}

}
