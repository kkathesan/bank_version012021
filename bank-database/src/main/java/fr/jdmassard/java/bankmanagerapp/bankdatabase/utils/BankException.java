package fr.jdmassard.java.bankmanagerapp.bankdatabase.utils;

/**
 * Class to define custom bank exception handler
 * @author Jean-Daniel MASSARD
 */
@SuppressWarnings("serial")
public class BankException extends Exception {

	public BankException() {
	}

	public BankException(String message) {
		super(message);
	}

	public BankException(Throwable cause) {
		super(cause);
	}

	public BankException(String message, Throwable cause) {
		super(message, cause);
	}

	public BankException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
