package fr.jdmassard.java.bankmanagerapp.bankdatabase.dao;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.User;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

public interface UserDAO {

  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
  public abstract void createUser(User user) throws BankException;

  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
  public abstract void updateUser(User user) throws BankException;

  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
  public abstract void deleteUser(User user) throws BankException;

  @Transactional(propagation = Propagation.REQUIRED)
  public abstract User fetchUserByUsername(String username) throws BankException;

  @Transactional(propagation = Propagation.REQUIRED)
  public abstract List<User> fetchUsers() throws BankException;

}
