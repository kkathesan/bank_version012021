package fr.jdmassard.java.bankmanagerapp.bankdatabase.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Account;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Client;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

/**
 * Implementation of bank account data access object.
 * @author Jean-Daniel MASSARD
 */
public class HibernateAccountDAO implements AccountDAO {
	
	/**
	 * Reference to the current session factory.
	 */
	private SessionFactory sessionFactory;

	/**
	 * Automatically sets the session factory from the bean configuration.
	 * @param sessionFactory Reference to the session factory itself.
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * Creates a new bank account in the database.
	 * @param account The account to create itself.
	 * @throws BankException
	 */
	public void createAccount(Account account) throws BankException {
		try {
			sessionFactory.getCurrentSession().persist(account);
		} catch (HibernateException exception) {
			throw new BankException("Cannot persist new account to the database.");
		}
	}
	
	/**
	 * Deletes an existing bank account from the database.
	 * @param account The account to delete itself.
	 * @throws BankException
	 */
	public void deleteAccount(Account account) throws BankException {
		try {
			sessionFactory.getCurrentSession().delete(account);
		} catch (Exception e) {
			throw new BankException("An error occured while deleting a bank account from the database.");
		}
	}
	
	/**
	 * Fetches all the bank accounts from the database.
	 * @return Returns all existing bank accounts.
	 * @throws BankException
	 */
	@SuppressWarnings("unchecked")
	public List<Account> fetchAccounts() throws BankException {
		try {
			String query = "from Account as a";
			return sessionFactory.getCurrentSession().createQuery(query).list();
		} catch (Exception e) {
			throw new BankException("An error occured while fetching all bank accounts from the database.");
		}
	}

	/**
	 * Fetches a specifc bank account by number.
	 * @param number The bank account number to fetch.
	 * @return Returns the specific bank account.
	 * @throws BankException
	 */
	public Account fetchAccountByNumber(long number) throws BankException {
		try {
			return (Account) sessionFactory.getCurrentSession().get(Account.class, number);
		} catch (HibernateException exception) {
			throw new BankException("Cannot fetch account n�" + number + ".");
		}
	}

	/**
	 * Fetches all bank accounts for a specific client.
	 * @param client The client to use to fetch the bank accounts list.
	 * @return Returns a list of bank accounts of the specified client.
	 * @throws BankException
	 */
	@SuppressWarnings("unchecked")
	public List<Account> fetchClientAccounts(Client client) throws BankException {
		try {
			String sql = "from Account as c where c.client=:client";
			return sessionFactory.getCurrentSession().createQuery(sql).setParameter("client", client).list();
		} catch (HibernateException exception) {
			exception.printStackTrace();
			throw new BankException("Cannot fetch all accounts for user id: " + client.getId());
		}
	}

	/**
	 * Updates an existing bank account in the database.
	 * @param account The account to update itself.
	 * @throws BankException
	 */
	public void updateAccount(Account account) throws BankException {
		try {
			sessionFactory.getCurrentSession().merge(account);
		} catch (HibernateException exception) {
			throw new BankException("Cannot update account n�" + account.getNumber() + ".");
		}
	}

}
