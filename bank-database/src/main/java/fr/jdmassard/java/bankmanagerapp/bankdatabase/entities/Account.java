package fr.jdmassard.java.bankmanagerapp.bankdatabase.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Basic class entity for bank account.
 * @author Jean-Daniel MASARD
 */
@Entity
@Table(name = "account")
public class Account {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "number")
	private long number;
	
	@Column(name = "balance")
	private double balance;
	
	@ManyToOne
	private Client client;

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	public String toString() {
		return String.format("Account[id=%d, balance='%s', client='%s']", number, balance, getClient().getUser().getUsername());
	}
	
}
