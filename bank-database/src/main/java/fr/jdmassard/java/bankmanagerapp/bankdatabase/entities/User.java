package fr.jdmassard.java.bankmanagerapp.bankdatabase.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Basic class entity for client.
 * @author Jean-Daniel MASSARD
 */
@Entity
@Table(name = "users")
public class User {
	
	@Id
	  @Column
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private Long id;

	  public Long getId() {
	    return id;
	  }

	  @Column(length = 50, nullable = false, unique = true)
	  private String username;

	  public String getUsername() {
	    return username;
	  }

	  public void setUsername(String username) {
	    this.username = username;
	  }

	  @Column(nullable = false)
	  private String password;

	  public String getPassword() {
	    return password;
	  }

	  public void setPassword(String password) {
	    this.password = password;
	  }

	  @Column(nullable = false)
	  private Boolean enabled;

	  public Boolean getEnabled() {
	    return enabled;
	  }

	  public void setEnabled(Boolean enabled) {
	    this.enabled = enabled;
	  }

	  @OneToOne(mappedBy = "user")
	  private Client client;

	  public Client getClient() {
	    return client;
	  }

	  @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
	  private List<Authority> authorities;

	  public List<Authority> getAuthorities() {
	    return authorities;
	  }

	  @Override
	  public String toString() {
	    return String.format("User[id=%d, username='%s', enabled='%s']", id, username, enabled);
	  }
	
}
