package fr.jdmassard.java.bankmanagerapp.bankdatabase.dao;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Account;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Client;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

/**
 * Basic interface for account data access object.
 * @author Jean-Daniel MASSARD
 */
public interface AccountDAO {
	
	/**
	 * Creates a new bank account in the database.
	 * @param account The account to create itself.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
	public abstract void createAccount(Account account)
			throws BankException;
	
	/**
	 * Updates an existing bank account in the database.
	 * @param account The account to update itself.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
	public abstract void updateAccount(Account account)
		throws BankException;
	
	/**
	 * Deletes an existing bank account from the database.
	 * @param account The account to delete itself.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BankException.class)
	public abstract void deleteAccount(Account account)
		throws BankException;
	
	/**
	 * Fetches all the bank accounts from the database.
	 * @return Returns all existing bank accounts.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract List<Account> fetchAccounts()
		throws BankException;
	
	/**
	 * Fetches a specifc bank account by number.
	 * @param number The bank account number to fetch.
	 * @return Returns the specific bank account.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract Account fetchAccountByNumber(long number)
			throws BankException;
	
	/**
	 * Fetches all bank accounts for a specific client.
	 * @param client The client to use to fetch the bank accounts list.
	 * @return Returns a list of bank accounts of the specified client.
	 * @throws BankException
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public abstract List<Account> fetchClientAccounts(Client client)
		throws BankException;
	
}
