package fr.jdmassard.bankportal.database.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.jdmassard.bankportal.database.entities.Authority;
import fr.jdmassard.bankportal.database.exceptions.BankException;

public class HibernateAuthorityDAO implements AuthorityDAO {

  @Autowired
  private SessionFactory sessionFactory;

  @Override
  public void createAuthority(Authority authority) throws BankException {
    try {
      sessionFactory.getCurrentSession().persist(authority);
    } catch (HibernateException exception) {
      throw new BankException("Cannot create the new authority in database.");
    }
  }

  @Override
  public void updateAuthority(Authority authority) throws BankException {
    try {
      sessionFactory.getCurrentSession().merge(authority);
    } catch (HibernateException exception) {
      throw new BankException("Cannot update authority of user: " + authority.getUsername() + ".");
    }
  }

  @Override
  public void deleteAuthority(Authority authority) throws BankException {
    try {
      sessionFactory.getCurrentSession().delete(authority);
    } catch (HibernateException exception) {
      throw new BankException("Cannot delete the authority from the database.");
    }
  }

  @Override
  public List<Authority> fetchAuthoritiesByUsername(String username) throws BankException {
    try {
      return sessionFactory.getCurrentSession().createQuery("from authorities as a where a.username=:username")
          .setParameter("username", username).list();
    } catch (HibernateException exception) {
      throw new BankException("Cannot load authorities for username: " + username + " from database.");
    }
  }

}
