package fr.kathesan.java;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.GsonBuilder;

import fr.jdmassard.java.bankmanagerapp.bankapplication.business.BankService;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.dao.AccountDAO;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.dao.AuthorityDAO;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.dao.ClientDAO;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.dao.UserDAO;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Account;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Authority;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Client;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.User;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

@Controller
public class ClientController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private ClientDAO clientDAO;

	@Autowired
	private AccountDAO accountDAO;
	
	@Autowired
	private UserDAO userDAO;

	@Autowired
	private AuthorityDAO authorityDAO;

	
	@Autowired
	private BankService bankService;

	int a = 0;

	// page  Admin
	// @RequestMapping(value="/accueil", method=RequestMethod.POST)
	@RequestMapping(value = "/admin/accueil", method = RequestMethod.GET)	//@RequestMapping(value = "/accueil", method = RequestMethod.GET)
	public String pageAccueil(Locale locale, Model model) {

		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "accueil";
	}
	
	// page User
	@RequestMapping(value = "/user/accueil", method = RequestMethod.GET)	
	public String pageAccueilUser(Locale locale, Model model) {

		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "accueilUser";
	}
	
	// page  Ajouter un client (admin)
	// @RequestMapping(value="/ajouterClient", method=RequestMethod.POST)
	@RequestMapping(value = "/admin/client/add", method = RequestMethod.GET)	//@RequestMapping(value = "/client/add", method = RequestMethod.GET)
	public String ajouterClient(Model model, Locale locale) {
		
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		
		return "addClient";
	}
	
	// @RequestMapping(value="/ajouterClient", method=RequestMethod.POST)
		@RequestMapping(value = "/admin/client/add", method = RequestMethod.POST)
		public String ajouterClientProcess(HttpServletRequest request, Model model) {
			
			User user = new User();
			user.setPassword(request.getParameter("password"));
			user.setEnabled(true);
			user.setUsername(request.getParameter("username"));
			
			Client c = new Client();
			c.setSurname(request.getParameter("surname"));
			c.setForename(request.getParameter("forename"));
			c.setAddress(request.getParameter("address"));
			c.setZip(request.getParameter("zip"));
			c.setCity(request.getParameter("city"));
			
			Authority userauthority = new Authority();
			userauthority.setAuthority("ROLE_USER");
			userauthority.setUsername(user.getUsername());	
			
			model.addAttribute("client", c);

			try {
				userDAO.createUser(user); // creer user
				
				c.setUser(user); // creer client
				clientDAO.createClient(c);
				
				userauthority.setUser(user); // autho pour user
				authorityDAO.createAuthority(userauthority);
				
				if(request.getParameter("subscribe") != null) {
					Authority adminauthority = new Authority();
					adminauthority.setAuthority("ROLE_ADMIN");
					adminauthority.setUsername(user.getUsername());	
					adminauthority.setUser(user); // autho pour user
					authorityDAO.createAuthority(adminauthority);
				}
					
			} catch (BankException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// return "result";
			//return "redirect:/client/listClients";
			return "redirect:/admin/client/addAccount/"+c.getId();	//return "redirect:/client/addAccount/"+c.getId();
		}
	
	// page  Modifier un client (admin)
	@RequestMapping(value = "/admin/client/modify/{client_id}", method = RequestMethod.GET)
	public String modifyClient(Model model, Locale locale, @PathVariable String client_id) {

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		try {
			model.addAttribute("client_form", clientDAO.searchClientByIdentifier(Long.parseLong(client_id)));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "modifyClient";
	}
	
	// @RequestMapping(value="/modifierClient", method=RequestMethod.POST)
			@RequestMapping(value = "/admin/client/modify/{client_id}", method = RequestMethod.POST)
			public String modifierClientProcess(HttpServletRequest request, Model model, @PathVariable String client_id) {
				String nom = request.getParameter("nom");
				String prenom = request.getParameter("prenom");
				String adresse = request.getParameter("adresse");
				String codepostal = request.getParameter("codepostal");
				String ville = request.getParameter("ville");
				String motdepasse = request.getParameter("motdepasse");
				String usernameString = request.getParameter("username");
				
				Client c; User user; Authority userauthority;
				try {
					user = userDAO.fetchUserByUsername(client_id);
					user.setPassword(request.getParameter("password"));
					user.setEnabled(true);
					user.setUsername(request.getParameter("username"));
					
					c = clientDAO.searchClientByIdentifier(Long.parseLong(client_id));
					c.setSurname(nom);
					c.setForename(prenom);
					c.setAddress(adresse);
					c.setZip(codepostal);
					c.setCity(ville);
					clientDAO.updateClient(c);
					
					
					
					
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (BankException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				// return "result";
				return "redirect:/admin/client/listClients";	//return "redirect:/client/listClients";
			}

	// page  Ajouter un compte (admin)
	@RequestMapping(value = "/admin/client/addAccount/{client_id}", method = RequestMethod.GET)
	public String ajouterCompte(Model model, Locale locale, @PathVariable String client_id) {
		
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		
		try {
			model.addAttribute("client_form", clientDAO.searchClientByIdentifier(Long.parseLong(client_id)));
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "addAccount";
	}

	// page  Faire un virement (user)
	@RequestMapping(value = "/user/client/doTransfer", method = RequestMethod.GET)	//@RequestMapping(value = "/client/doTransfer", method = RequestMethod.GET)
	public String faireVirement(Model model, Locale locale, Authentication authentication) {
		
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		
		User user = getUsernameFromBDD(authentication.getName());
		System.out.print(user);
		
		try {
			model.addAttribute("accountList", accountDAO.fetchAccounts());
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "virement";
	}
	
	public User getUsernameFromBDD(String username) {
        try {
            User user = userDAO.fetchUserByUsername(username);
            return  user;
        } catch (BankException e) {
            e.printStackTrace();
        }
        return null;
    }
	
	// page  Faire un virement (admin)
		@RequestMapping(value = "/admin/doTransfer", method = RequestMethod.GET)	//@RequestMapping(value = "/client/doTransfer", method = RequestMethod.GET)
		public String faireVirement2(Model model, Locale locale, Authentication authentication) {
			
			logger.info("Welcome home! The client locale is {}.", locale);

			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

			String formattedDate = dateFormat.format(date);

			model.addAttribute("serverTime", formattedDate);
			
			User user = getUsernameFromBDD(authentication.getName());
			System.out.print(user);
			
			try {
				model.addAttribute("accountList", accountDAO.fetchAccounts());
			} catch (BankException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "virement2";
		}

	// page  Liste des clients et leurs comptes (admin)
	@RequestMapping(value = "/admin/client/listClients", method = RequestMethod.GET)	//@RequestMapping(value = "/client/listClients", method = RequestMethod.GET)
	public String listeClient(Model model, Locale locale) {

		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		
		try {
			List<Account> accounts = accountDAO.fetchAccounts();
			Map<Object, List<Account>> var = accounts.stream().collect(Collectors.groupingBy(account -> account.getClient().getId()));
			model.addAttribute("accountListByClient", var);
			
			model.addAttribute("clientList", clientDAO.fetchClients());
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "listClient";
	}
	
	// page  Liste des clients sans leurs comptes (admin)
	@RequestMapping(value = "/admin/client/listClientSolo", method = RequestMethod.GET)	//@RequestMapping(value = "/client/listClientSolo", method = RequestMethod.GET)
	public String listClientSolo(Locale locale, Model model) {

		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		
		try {
			List<Account> accounts = accountDAO.fetchAccounts();
			Map<Object, List<Account>> var = accounts.stream().collect(Collectors.groupingBy(account -> account.getClient().getId()));
			model.addAttribute("accountListByClient", var);
			
			model.addAttribute("clientList", clientDAO.fetchClients());
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "listClientSolo";
	}
	
	// page  Liste de tous les comptes client (user)
	@RequestMapping(value = "/admin/client/listAccountSolo", method = RequestMethod.GET)	//@RequestMapping(value = "/client/listAccountSolo", method = RequestMethod.GET)
	public String listAccountSolo(Locale locale, Model model) {

		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		
		try {
			List<Account> accounts = accountDAO.fetchAccounts();
			Map<Object, List<Account>> var = accounts.stream().collect(Collectors.groupingBy(account -> account.getClient().getId()));
			model.addAttribute("accountListByClient", var);
			
			model.addAttribute("clientList", clientDAO.fetchClients());
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "listAccountSolo";
	}

	// Liste des comptes (User)
	@RequestMapping(value = "/user/client/listAccountSoloUser", method = RequestMethod.GET)
	public String listAccountUser(Locale locale, Model model) {

		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		
		try {
			List<Account> accounts = accountDAO.fetchAccounts();
			Map<Object, List<Account>> var = accounts.stream().collect(Collectors.groupingBy(account -> account.getClient().getId()));
			model.addAttribute("accountListByClient", var);
			
			model.addAttribute("clientList", clientDAO.fetchClients());
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "listAccountSoloUser";
	}

	@ModelAttribute("clientParDefaut")
	public Client clientParDefaut() {
		Client c1 = new Client();
		c1.setForename("Dubois");
		c1.setSurname("Pierre");
		c1.setAddress("Rue de limayrac");
		c1.setZip("31000");
		c1.setCity("Toulouse");

		return c1;
	}

	// @RequestMapping(value="/ajouterCompteClient", method=RequestMethod.POST)
	@RequestMapping(value = "/admin/client/addAccount/{client_id}", method = RequestMethod.POST)
	public String ajouterCompteProcess(HttpServletRequest request, Model model, @PathVariable String client_id) {

		String balance = request.getParameter("balance");
		//String clientId = request.getParameter("clientId");
		String clientId = client_id;
		
		try {
			Account account = new Account();
			account.setBalance(Double.parseDouble(balance));
			account.setClient(clientDAO.searchClientByIdentifier(Long.parseLong(clientId)));
			accountDAO.createAccount(account);
		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (BankException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// return "result";
		return "redirect:/admin/client/listClients";	//return "redirect:/client/listClients";
	}

	// @RequestMapping(value="/faireVirement", method=RequestMethod.POST)
	@RequestMapping(value = "/user/client/doTransfer", method = RequestMethod.POST)	//@RequestMapping(value = "/client/doTransfer", method = RequestMethod.POST)
	public String faireVirementProcess(HttpServletRequest request, Model model) {

		String fromAccountID = request.getParameter("fromAccountID");
		String toAccountID = request.getParameter("toAccountID");
		String priceToTransfer = request.getParameter("priceToTransfer");
		try {
			bankService.bankTransfer(Long.parseLong(fromAccountID), Long.parseLong(toAccountID),
					Double.parseDouble(priceToTransfer));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//return "result";
		return "redirect:/user/client/doTransfer";	//return "redirect:/client/doTransfer";
	}

	// @RequestMapping(value="/listeDesClients", method=RequestMethod.POST)
	@RequestMapping(value = "/admin/client/listClients", method = RequestMethod.POST)
	public String listClientProcess(HttpServletRequest request, Model model) {

		Account compte = new Account();

		System.out.println("client = " + compte);
		model.addAttribute("client", compte);

		return "result";
		// return "redirect:/";
	}
	
	@RequestMapping(value = "/admin/client/listClientSolo", method = RequestMethod.POST)
	public String listClientSoloProcess(HttpServletRequest request, Model model) {

		Account compte = new Account();

		System.out.println("client = " + compte);
		model.addAttribute("client", compte);

		return "result";
		// return "redirect:/";
	}

	@RequestMapping(value = "/admin/client/listAccountSolo", method = RequestMethod.POST)		//@RequestMapping(value = "/client/listAccountSolo", method = RequestMethod.POST)
	public String listAccountSoloProcess(HttpServletRequest request, Model model) {

		Account compte = new Account();

		System.out.println("client = " + compte);
		model.addAttribute("client", compte);

		return "result";
		// return "redirect:/";
	}
	
	@RequestMapping(value = "/user/client/listAccountSolo", method = RequestMethod.POST)		//@RequestMapping(value = "/client/listAccountSolo", method = RequestMethod.POST)
	public String listAccountSoloProcessUser(HttpServletRequest request, Model model) {

		Account compte = new Account();

		System.out.println("client = " + compte);
		model.addAttribute("client", compte);

		return "result";
		// return "redirect:/";
	}
}
