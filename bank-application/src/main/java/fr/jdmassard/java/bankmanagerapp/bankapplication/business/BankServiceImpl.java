package fr.jdmassard.java.bankmanagerapp.bankapplication.business;

import java.util.List;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.dao.AccountDAO;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.dao.ClientDAO;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Account;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Client;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

public class BankServiceImpl implements BankService {
	
	private ClientDAO clientDAO;
	private AccountDAO accountDAO;

	public void setClientDAO(ClientDAO clientDAO) {
		this.clientDAO = clientDAO;
	}

	public void setAccountDAO(AccountDAO accountDAO) {
		this.accountDAO = accountDAO;
	}

	public Client authenticate(String identifier, String password) throws BankException {
		try {
			Client client = clientDAO.searchClientByIdentifier(Long.parseLong(identifier));
			/*if (client != null && client.getPassword().equals(password)) {
				return client;
			} else {
				throw new BankException();
			}*/
			return client;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankException("Cannot authenticate the user");
		}
	}

	public List<Account> fetchClientAccounts(long clientId) throws BankException {
		try {
			Client client = clientDAO.searchClientByIdentifier(clientId);
			return accountDAO.fetchClientAccounts(client);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankException("An error occured whit searching for user accounts");
		}
	}

	public void bankTransfer(long accountIdToDebit, long accountIdToCredit, double amount) throws BankException {
		try {
			Account accountToDebit = accountDAO.fetchAccountByNumber(accountIdToDebit);
			Account accountToCredit = accountDAO.fetchAccountByNumber(accountIdToCredit);
			
			accountToDebit.setBalance((Double)(accountToDebit.getBalance() - amount));
			accountToCredit.setBalance(accountToCredit.getBalance() + amount);
			
			accountDAO.updateAccount(accountToDebit);
			accountDAO.updateAccount(accountToCredit);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankException("An error occured while transfering money between two accounts");
		}
	}

}
