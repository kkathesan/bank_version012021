package fr.jdmassard.java.bankmanagerapp.bankapplication.aspects;

import org.aspectj.lang.JoinPoint;

public class Logging {

	public void beforeMethod(JoinPoint joinPoint) {
		// Prepare some variables
		String methodName = joinPoint.getSignature().toShortString();
		Object[] args = joinPoint.getArgs();
		
		// Build the log
		StringBuilder builder = new StringBuilder();
		builder.append("Method ");
		builder.append(methodName);
		builder.append(" invoked with parameters: ");
		for (Object arg : args) {
			builder.append(arg);
			builder.append(" - ");
		}
		
		System.out.println(builder.toString());
	}
	
	public void afterMethod(JoinPoint joinPoint, Object result) {
		String methodName = joinPoint.getSignature().toShortString();
		System.out.println("Method " + methodName + " generated the result " + result);
	}
	
}
