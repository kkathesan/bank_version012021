package fr.jdmassard.java.bankmanagerapp.bankapplication.business;

import java.util.List;

import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Account;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.entities.Client;
import fr.jdmassard.java.bankmanagerapp.bankdatabase.utils.BankException;

public interface BankService {

	public abstract Client authenticate(String identifier, String password)
		throws BankException;
	
	public abstract List<Account> fetchClientAccounts(long clientId)
		throws BankException;
	
	public abstract void bankTransfer(long accountIdToDebit, long accountIdToCredit, double amount)
		throws BankException;
	
}
